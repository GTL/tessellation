


var result = [];
var index = [];
var input  = [];
var canvas = null;
var ctx;

var Clear = function(){
    //Clear parimeter and poly buffer
    result = [];
    input  = [];
    
    //Clear Canvas
    canvas.width = canvas.width;
}

var DrawPolys = function(){
    //Calculate Polys
    Triangulate.Process(input, result, index);
    
    var totalPolys = result.length/3;
    
    //Draw Polys
    ctx.strokeStyle = 'blue';
    for(var i = 0; i < totalPolys; i++){
        ctx.beginPath();
        ctx.moveTo(result[i*3 + 0][0], result[i*3 + 0][1]);
        ctx.lineTo(result[i*3 + 1][0], result[i*3 + 1][1]);
        ctx.lineTo(result[i*3 + 2][0], result[i*3 + 2][1]);
        ctx.closePath();
        ctx.stroke();
    }
}

var DrawContour = function(){
    //Clear Canvas
    canvas.width = canvas.width;
    
    //Draw Contour
    ctx.beginPath();
    ctx.strokeStyle = 'red';
    ctx.moveTo(input[0][0], input[0][1]);
    for(var i = 1; i < input.length; i++){
        ctx.lineTo(input[i][0], input[i][1]);
    }
    ctx.closePath();
    ctx.stroke();
}
var PlotPoint = function(e){
    var point = canvas.relMouseCoords(e);
    input.push([point.x, point.y]);
    
    DrawContour();
    
}

var SaveOBJ = function(){
    var filename = prompt("Enter Model Name");
    
    if(index.length > 0){
        
        var file = "#Created By GameTheoryLabs\n";
        
        for(var i = 0; i < input.length; i++){
            file += "v " + input[i][0] + " " + input[i][1] + " " + "0\n";
        }
        
        var numOfPolys = result.length/3;
        
        for(var j = 0; j < numOfPolys; j++){
            file += "f " + index[j*3 + 0] + " " + index[j*3 + 1] + "  " + index[j*3 + 2] + "\n";
        }
        var blob = new Blob([file]);
        saveAs(blob, filename +".obj");
        
    }
}

var LaunchViewer = function(){
    document.location.href = "http://viewer.gametheorylabs.com";
}

var Init = function(){
    canvas = document.getElementById("canvas");
    ctx = canvas.getContext('2d');
    
    canvas.onclick =PlotPoint;
    
    //Initial Contour
    input.push([0,120]);
    input.push([0,0]);
    input.push([60,0]);
    input.push([80,20]);
    input.push([120,20]);
    input.push([160,0]);
    input.push([240,0]);
    input.push([260,40]);
    input.push([160,40]);
    input.push([160,80]);
    input.push([220,80]);
    input.push([220,120]);
    input.push([120,120]);
    input.push([80,60]);
    input.push([40,120]);
    
    DrawContour();
    
}

function relMouseCoords(event){
    var totalOffsetX = 0;
    var totalOffsetY = 0;
    var canvasX = 0;
    var canvasY = 0;
    var currentElement = this;

    do{
        totalOffsetX += currentElement.offsetLeft - currentElement.scrollLeft;
        totalOffsetY += currentElement.offsetTop - currentElement.scrollTop;
    }
    while(currentElement = currentElement.offsetParent)

    canvasX = event.pageX - totalOffsetX;
    canvasY = event.pageY - totalOffsetY;

    return {x:canvasX, y:canvasY}
}

HTMLCanvasElement.prototype.relMouseCoords = relMouseCoords;