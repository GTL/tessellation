var Triangulate = {
    EPSILON: 0.0000000001,
    Process: function(contour, result, index){

        var n = contour.length;
        
        if(n < 3){return false}
        
        var V = [];
        
        if(0.0 < Triangulate.Area(contour)){
            for(var v = 0; v < n; v++){V[v] = v}
        }
        else{
            for(var v = 0; v < n; v++){V[v] = (n-1)-v}
        }
            
        var nv = n;
        
        var count = 2*nv;
        
        for(var m=0, v=nv-1; nv > 2;){
            if(0 >= (count--)){
                return false;
            }
            var u = v; if(nv <= u) u = 0;
            v = u + 1; if(nv <= v) v = 0;
            var w = v + 1; if (nv < w) w = 0;
            
            if( Triangulate.Snip(contour, u,v,w,nv,V)){
                var a,b,c,s,t;
                
                a = V[u], b = V[v], c = V[w];
                
                if(index){
                    index.push(a + 1);
                    index.push(b + 1);
                    index.push(c + 1);
                }
                result.push(contour[a]);
                result.push(contour[b]);
                result.push(contour[c]);
                
                m++;
                
                for(s=v, t=v+1; t < nv; s++, t++){
                    V[s] = V[t];
                }
                
                nv--;
                count = 2 * nv;
            }
        }
        V = null;
        return true;
    },
    Area: function(contour){
        
        var area = 0.0;
        
        var n = contour.length;
        var q = 0;
        
        for(var p = n - 1; q < n; p=q++){
            area += (contour[p][0] * contour[q][1]) - (contour[q][0] * contour[p][1]);
        }
        
        return area * 0.5;
    },
    InsideTriangle: function(A, B, C, P){
        var ax, ay, bx, by, cx, cy, apx, apy, bpx, bpy, cpx, cpy;
        var cCROSSap, bCROSScp, aCROSSbp;
      
        ax = C[0]  - B[0] ;  ay = C[1]  - B[1];
        bx = A[0]  - C[0] ;  by = A[1]  - C[1];
        cx = B[0]  - A[0] ;  cy = B[1]  - A[1];
        apx= P[0]  - A[0] ;  apy= P[1]  - A[1];
        bpx= P[0]  - B[0] ;  bpy= P[1]  - B[1];
        cpx= P[0]  - C[0] ;  cpy= P[1]  - C[1];
      
        aCROSSbp = ax*bpy - ay*bpx;
        cCROSSap = cx*apy - cy*apx;
        bCROSScp = bx*cpy - by*cpx;
      
        return ((aCROSSbp >= 0.0) && (bCROSScp >= 0.0) && (cCROSSap >= 0.0));
    },
    Snip: function(contour, u,v,w,n,V){
        var p;
        var A = [], B = [], C = [], P = [];
      
        A[0] = contour[V[u]][0];
        A[1] = contour[V[u]][1];
      
        B[0] = contour[V[v]][0];
        B[1] = contour[V[v]][1];
      
        C[0] = contour[V[w]][0];
        C[1] = contour[V[w]][1];
      
        if ( Triangulate.EPSILON > (((B[0]-A[0])*(C[1]-A[1])) - ((B[1]-A[1])*(C[0]-A[0]))) ) return false;
      
        for (p=0;p<n;p++)
        {
          if( (p == u) || (p == v) || (p == w) ) continue;
          P[0] = contour[V[p]][0];
          P[1] = contour[V[p]][1];
          if (Triangulate.InsideTriangle(A,B,C,P)) return false;
        }
      
        return true;
    }
    
}